# the tests are in the lagranto package


def test_lagranto_imports():
    # only test that everything can be imported

    from dypy.lagranto import Tra, LagrantoRun
    from dypy.lagranto import LagrantoException
    from dypy.lagranto import hhmm2frac
