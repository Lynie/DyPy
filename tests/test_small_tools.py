import os
import numpy as np
import numpy.ma as ma
from numpy.testing import assert_almost_equal

from dypy.small_tools import (read_shapefile, mask_polygon,
                              potential_temperature, CrossSection,
                              great_circle_distance, rotate_points,
                              return_erainterim_pressure)

testdatadir = os.path.join(os.path.dirname(__file__), 'test_data')


def test_read_shapefile_single_polygon():
    shapefile = os.path.join(testdatadir, 'shapefile_single_polygon.shp')
    pnts, projections = read_shapefile(shapefile)
    test_pnts = np.array([[8.61686488, 46.08349113],
                          [8.47050174, 45.98639637],
                          [8.34168295, 46.22408627],
                          [8.48540242, 46.29762834],
                          [8.61686488, 46.08349113]])
    assert_almost_equal(pnts[0], test_pnts)
    assert projections == "+init=epsg:4326"


def test_mask_polygon():
    array = np.zeros((20, 20)) + 10.
    polygon = np.array([[5, 5],
                        [10, 5],
                        [10, 10],
                        [5, 10],
                        [5, 5]])
    lon = np.arange(0, 20, dtype=float)
    lat = np.arange(0, 20, dtype=float)
    lon, lat = np.meshgrid(lon, lat)

    clipped = mask_polygon(polygon, array, lon, lat)

    test_clip = np.zeros((20, 20))
    test_clip[5:10, 5:10] = 10
    test_clip = ma.masked_not_equal(test_clip, 10)

    assert_almost_equal(clipped, test_clip)


def test_potential_temperature():
    t = 15. + 273.15  # K
    p = 750.  # hPa
    th = potential_temperature(t, p)
    assert_almost_equal(th, 312.83, decimal=1)


def test_crosssection():
    rlon = np.arange(0, 20, 1)
    rlat = np.arange(0, 20, 1)
    qv = np.zeros((10, 20, 20))
    qv[:, :, 9:11] = 10
    coo = [(24, 46.07452973), (31, 64.3)]
    variables = {'rlon': rlon,
                 'rlat': rlat,
                 'qv': qv}
    cross = CrossSection(variables, coo, np.arange(0, 10))
    distance = great_circle_distance(coo[0][0], coo[0][1],
                                     coo[1][0], coo[1][1])
    assert cross.distance == distance
    np.testing.assert_allclose(cross.qv, np.ones_like(cross.qv) * 10)


def test_crosssection_regular():
    lon = np.arange(10, 21, 1)
    lat = np.arange(40, 51, 1)
    qv = np.zeros((10, 11, 11))
    qv[:, :, 3:6] = 10
    coo = [(14, 40), (14, 50)]
    variables = {'lon': lon,
                 'lat': lat,
                 'qv': qv}
    cross = CrossSection(variables, coo, np.arange(0, 10), version='regular')
    np.testing.assert_allclose(cross.qv, np.ones_like(cross.qv) * 10)


def test_great_circle():
    assert great_circle_distance(0, 55, 8, 45.5) == 1199.3240879770135


def test_rotate_points():
    rlon, rlat = rotate_points(-170, 43, 8, 47)
    np.testing.assert_allclose(rlon, np.array([-1.36384859]))
    np.testing.assert_allclose(rlat, np.array([0.01740901]))
    lon, lat = rotate_points(-170, 43, 0, 0, direction='r2n')
    np.testing.assert_allclose(lon, np.array([10.]))
    np.testing.assert_allclose(lat, np.array([47.0]))


def test_return_erainterim_pressure():
    """Based on
    https://www.ecmwf.int/sites/default/files/elibrary/2011/
        8174-era-interim-archive-version-20.pdf
    Page 4
    """
    good_p = np.array([1.01204936e+03, 1.00905633e+03, 1.00464373e+03,
                       9.98385381e+02, 9.89943524e+02, 9.79063311e+02,
                       9.65567236e+02, 9.49349398e+02, 9.30370236e+02,
                       9.08650546e+02, 8.84266040e+02, 8.57341837e+02,
                       8.28046849e+02, 7.96587866e+02, 7.63204470e+02,
                       7.28163120e+02, 6.91751520e+02, 6.54273152e+02,
                       6.16041722e+02, 5.77375407e+02, 5.38591330e+02,
                       5.00000018e+02, 4.61899652e+02, 4.24570629e+02,
                       3.88269963e+02, 3.53225577e+02, 3.19630688e+02,
                       2.87638340e+02, 2.57355744e+02, 2.28838667e+02,
                       2.02085836e+02, 1.77117589e+02, 1.53995132e+02,
                       1.32757664e+02, 1.13421210e+02, 9.59780627e+01,
                       8.03968507e+01, 6.66232605e+01, 5.46236343e+01,
                       4.43349915e+01, 3.57835655e+01, 2.88815498e+01,
                       2.33108120e+01, 1.88145714e+01, 1.51855755e+01,
                       1.22565460e+01, 9.89247513e+00, 7.98439217e+00,
                       6.44434547e+00, 5.20134640e+00, 4.19295025e+00,
                       3.36233878e+00, 2.66637444e+00, 2.07681704e+00,
                       1.57533824e+00, 1.15060127e+00, 7.96423793e-01,
                       5.10365665e-01, 2.92126685e-01, 9.99999940e-02])
    p = return_erainterim_pressure(np.array([1013.25])).squeeze()
    np.testing.assert_allclose(p, good_p)
