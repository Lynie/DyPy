import matplotlib as mpl
import numpy as np

mpl.rcdefaults()
import matplotlib.pyplot as plt
from PIL import Image
from path import Path
from dypy.plotting import Mapfigure
from dypy.lagranto import Tra
import pytest
from mpl_toolkits.basemap import Basemap

testdatadir = Path.dirname(Path(__file__)).joinpath('test_data')


@pytest.mark.slow
def test_plot_trajs():
    picfile = testdatadir.joinpath('images_plot_trajs.png')
    if picfile.isfile():
        ctrl_array = np.array(Image.open(picfile))
    filename = testdatadir.joinpath('lsl_lagranto2_0.nc')
    trajs = Tra(filename)

    m = Mapfigure(domain=[0, 15, 30, 55], resolution='l')

    fig, ax = plt.subplots()
    m.ax = ax
    m.drawcoastlines()
    m.plot_traj(trajs, 'QV')
    if picfile.isfile():
        npicfile = picfile.replace(picfile.ext, '_test' + picfile.ext)
        fig.savefig(npicfile)
        test_array = np.array(Image.open(npicfile))
        np.testing.assert_almost_equal(ctrl_array, test_array)
        Path(npicfile).remove()
    else:
        fig.savefig(picfile)


def test_Mapfigure_instance():
    m = Mapfigure(domain=[0, 15, 30, 55], resolution='l')
    m2 = Mapfigure(basemap=Basemap(llcrnrlat=3, llcrnrlon=0, urcrnrlat=55,
                                     urcrnrlon=15, resolution='l'))
    assert m.proj4string == m2.proj4string
