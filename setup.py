#!/usr/bin/env python
# coding: utf-8

import os
from glob import glob
from os.path import splitext, basename

try:
    from setuptools import setup, find_packages
except ImportError:
    from distutils.core import setup

__AUTHOR__ = 'Nicolas Piaget'
__AUTHOR_EMAIL__ = 'nicolas.piaget@env.ethz.ch'

readme = open('README.rst').read()  # + '\n\n' + open('CHANGELOG.rst').read()

on_rtd = os.environ.get('READTHEDOCS') == 'True'
if on_rtd:
    requirements = ['path.py', 'docopt', 'Jinja2']
else:
    requirements = ['Cartopy', 'docopt', 'docutils', 'Fiona', 'Lagranto',
                    'Jinja2', 'matplotlib', 'netCDF4', 'numpy', 'pandas',
                    'path.py', 'Pillow', 'scipy', 'Shapely', 'basemap']

tests_require = ['pytest']

setup(name='DyPy',
      version='0.1.8',
      author=__AUTHOR__,
      author_email=__AUTHOR_EMAIL__,
      maintainer=__AUTHOR__,
      maintainer_email=__AUTHOR_EMAIL__,
      description='Collection of tools for atmospheric science',
      long_description=readme,
      setup_requires=['pytest-runner>=2.0'],
      dependency_links=[
          'git+https://github.com/matplotlib/basemap.git@master#egg=basemap-1.0.0'
      ],
      packages=find_packages('src', exclude=['docs', 'tests*']),
      package_dir={'': 'src'},
      py_modules=[splitext(basename(path))[0] for path in glob('src/*.py')],
      install_requires=requirements,
      tests_require=tests_require,
      extras_require={
          'docs':  ['Sphinx', 'sphinx-rtd-theme'],
          'testing': tests_require,
      },
      include_package_data=True,
      classifiers=[
          'Development Status :: 4 - Beta',
          'Programming Language :: Python :: 2.7',
          'Programming Language :: Python :: 3.4',
      ]
      )
